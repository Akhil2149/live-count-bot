
__version__ = '0.0.1a1'
__author__ = 'AXVin'
__description__ = 'A Discord Bot for displaying and flexing about your analytics!'

import discord
from discord.ext import commands

import sys, traceback
from cogs.utils import config, context
import aiohttp
import asyncio
from datetime import datetime
import logging

logger = logging.getLogger('discord')
logger.setLevel(logging.ERROR)
handler = logging.FileHandler(filename=f'logs/{datetime.utcnow()}.log', encoding='utf-8', mode='w')
handler.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s'))
logger.addHandler(handler)


description='The Live Count Bot!'


OWNER = 310074794102095873


def get_prefix(bot, message):
    """A callable Prefix for our bot. This could be edited to allow per server prefixes."""

    prefixes = ['l!', 'l! ']
    if not message.guild:
        return 'l!'
    if message.author.id == OWNER:
        prefixes.append('')

    return commands.when_mentioned_or(*prefixes)(bot, message)


initial_extensions = (
    'cogs.owner',
    'cogs.miscellaneous',
    'cogs.twitch',
    'cogs.setup',
    'cogs.debug',
    # 'cogs.dbl',
    'cogs.live_count',
    'cogs.faq',
    'jishaku'
)


class LiveCount(commands.AutoShardedBot):

    def __init__(self):
        super().__init__(command_prefix=get_prefix, description=description)
        self.owner_id = 310074794102095873
        self._task = self.loop.create_task(self.initialize())
        # self.spam_control = commands.CooldownMapping.from_cooldown(10, 12, commands.BucketType.user)
        self.activity = discord.Game(name='l!info | l!help')
        for extension in initial_extensions:
            try:
                self.load_extension(extension)
            except:
                print(f'Failed to load extension {extension}.', file=sys.stderr)
                traceback.print_exc()


    async def initialize(self):
        self.session = aiohttp.ClientSession(loop=self.loop)
        await self.wait_until_ready()
        self.owner = self.get_user(self.owner_id)


    async def process_commands(self, message):
        ctx = await self.get_context(message, cls=context.Context)
        if ctx.command is None:
            return
        if message.author.id != self.owner_id:
            await message.channel.send("Sorry, this bot isn't public yet!")
            return
        await self.invoke(ctx)


    async def on_message(self, message):
        if message.author.bot:
            return
        await self.process_commands(message)


    async def on_ready(self):
        total_width = 0
        infos = (
            'Live Count Bot',
            f'{self.user.name} [{self.user.id}]',
            f'Discord: {discord.__version__}',
            f'Guilds: {len(self.guilds)}',
            f'Users: {len(self.users)}',
            f'Shards: {self.shard_count}'
        )
        for info in infos:
            width = len(str(info)) + 4
            if width > total_width:
                total_width = width

        sep = '+'.join('-' * int((total_width/2)+1))
        sep = f'+{sep}+'

        information = [sep]
        for info in infos:
            elem = f'|{info:^{total_width}}|'
            information.append(elem)
        information.append(sep)
        print('\n'.join(information))

        
        # print(f'\n\nLogged in as: {self.user.name} - {self.user.id}\nVersion: {discord.__version__}\n\n'
        #       f'Guilds: {len(self.guilds)}\nUsers: {len(self.users)}\n'
        #       f'Shards: {self.shard_count}\n\n')

        print(f'Successfully logged in and booted...!')


    async def on_resumed(self):
        print('resumed...')


    async def close(self):
        await super().close()
        await self.session.close()
        self._task.cancel()


    def run(self):
        try:
            super().run(config.token, bot=True, reconnect=True)
        except Exception as e:
            print(f'Troubles running the bot!\nError: {e}')
            # traceback.print_exc()

def main():
    bot = LiveCount()
    bot.run()


if __name__ == '__main__':
    main()
