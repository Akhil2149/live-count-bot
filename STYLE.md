# Python Coding Style

The general rule is:

 - Write code in a manner similar to that around it.

This document describes guidelines that people new to the codebase tend not to immediately pick up on.  Obviously, these aren't universal rules (besides rule 1) and situations might require your better judgement.

1. Follow PEP8 at all times. Exceptions could be made at some places where readability is sacrificed due to it or as  specified here!

1. All lines must not exceed 75 characters(as opposed to PEP8's 71 char)
> Reason: @AXVin's editor window is that long

1. You can word-wrap your function parameters, put them all on separate lines at the same indentation level, but don't put them "almost" all on separate lines at the same indentation.

> E.g. don't do this, especially:
> ```python
my_func(arg1,
        arg2,
        arg3,
        arg4, arg5)
```
> This is desirable:
>
> ```python
my_func(arg1,
        arg2,
        arg3,
        arg4,
        arg5)
```
> Reason: People are more likely to overlook the unusually placed parameter.

1. Use indentation, don't put control statements like `: pass` or `: return` in the same line as other things.

> Reason: 
> * Quick scan-ability of control flow is useful for checking correctness w.r.t. certain classes of bugs.
> * PEP8 specification.
> * The rest of the code is that way.


1. Don't casually use bad and non-async libraries.

> Reason:
> * Some libraries greatly increase build times.
> * Synchronous code in an asynchronous application leads to blocking

1. Don't do wierd imports like `from discord import *` or `from discord.ext.commands import Bot`
> Reason:
> * These imports mess up the namespace of the module
> * It creates confusion in the minds of the reader
  
1. Avoid excessive dependency entanglements.  Prefer not to declare publicly used classes inside other classes.

> Reason: Declaring classes inside other classes means the class cannot be forward declared, which makes refactoring header file dependencies much more work.
  
> You could declare the classes inside a namespace instead.

1. Write comments that make it look like you made an effort to be professional and communicative.

> Commenting what every field is/does in a type is often a good idea.

1. Write commit messages that are not unintelligible muttering. Do not commit a change without testing it.

> High standards here would discourage frequent committing.

1. All commit messages must be in present tense not past. For eg: `Adds bleh feature` or `Fixes bugs`

> This is to recognise differences easily at a glance
