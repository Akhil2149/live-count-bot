import discord
from discord.ext import commands
from rethinkdb import RethinkDB
from .utils import main, db
from .utils import twitch_utils as tu

class Setup(commands.Cog):
    """Setup commamds for Live Count Bot"""
    def __init__(self, bot):
        self.bot = bot



    @commands.group(aliases=["tset", "ts"], invoke_without_command=True)
    @commands.guild_only()
    @commands.has_permissions(manage_guild=True)
    async def twitchset(self, ctx):
        """Commands for setting information about Twitch Live Count!"""
        await ctx.send_help(ctx.command)




    @twitchset.command()
    @commands.guild_only()
    @commands.has_permissions(manage_guild=True)
    async def client_id(self, ctx, client_id:str):
        """
        [!important] Set the client id which will be used for twitch live counts.
        This is sensitive information. Please use it in a private channel!
        See `l!faq twitch` for more information!
        """
        try:
            await ctx.message.delete()
        except discord.errors.Forbidden: # If can't delete message then ignore
            pass
        await db.check_default_guild_settings(ctx.guild)
        try:
            await self.set_twitch_client_id(ctx.guild, client_id)
        except main.InvalidSecret:
            await ctx.send("Invalid client-id! Please use a valid one!")
            return
        await ctx.send("Twitch Client ID set successfully!")




    @twitchset.command()
    @commands.guild_only()
    @commands.has_permissions(manage_guild=True)
    async def setup(self, ctx, target:str, streamer:str, voice_channel:discord.VoiceChannel=None):
        '''
        Sets Up Twitch Count 
        See `l!faq twitch` for more info
        '''
        targets = {
            'followers': 'Twitch Followers: {count}',
            'views': 'Twitch Views: {count}'
        }
        if not target.lower() in targets.keys():
            await ctx.send('Target must either be `followers` or `views`')
        member = ctx.author
        guild = ctx.guild
        await db.check_default_guild_settings(guild)
        client_id = await tu.get_twitch_client_id(guild)
        streamer = await tu.get_twitch_streamer(ctx, client_id, streamer)

        r, connection = await ctx.acquire()
        document = await r.table('guilds'
                                ).get(str(guild.id)
                                ).pluck(
                                {
                                    'connections': {
                                        'twitch': True
                                    }
                                }).run(connection)
        if len(document['connections']['twitch']) > 0:
            await ctx.send('Only one twitch live count is allowed per server!')
            return

        if not voice_channel and member.voice:
            voice_channel = member.voice.channel
        elif not voice_channel and not member.voice:
            try:
                voice_channel = await guild.create_voice_channel(name=targets[target])
            except Exception as e:
                await ctx.send(e)
                return

        guild_insertion = {
            "connections": {
                "twitch": [{
                    "channel": str(voice_channel.id),
                    "type": target,
                    "streamer_id": streamer['id']
                }]
            }
        }
        streamer_insertion = {
            "connections": [{
                "channel": str(voice_channel.id), 
                "type": target,
                "guild": str(guild.id),
                "format": targets[target]
            }]
        }

        await r.table('guilds'
                      ).get(str(guild.id)
                      ).update(guild_insertion
                      ).run(connection)
        await r.table('twitch'
                      ).get(streamer['id']
                      ).update(streamer_insertion
                      ).run(connection)


        title = 'Successfully completed the setup!'
        description = ('The channel will be updated on next '
                       'update cycle if any changes are found!')
        embed = discord.Embed(title=title,
                              description=description,
                              color=discord.Color.green())
        try:
            await ctx.send(embed=embed)
        except:
            await ctx.send(f'{title}\n{description}')



    




    async def set_twitch_client_id(self, guild:discord.Guild, client_id:str):
        valid = await tu.validate_twitch_client_id(self.bot.session, client_id)
        r, connection = await db.acquire()
        await r.table("guilds"
        ).get(str(guild.id)
        ).update({"secrets":{"twitch": client_id}}
        ).run(connection)
        await connection.close()



def setup(bot):
    cog = Setup(bot)
    bot.add_cog(cog)
