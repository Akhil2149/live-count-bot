import json

import discord
from discord.ext import commands

from cogs.utils.paginator import FieldPages



class FAQ(commands.Cog):
    '''
    The Frequently Asked Questions about the bot!
    '''

    def __init__(self, bot):
        self.bot = bot
        with open('faq.json', encoding='utf-8', mode="r") as file:
            self.faq_data = json.load(file)
        self.general_faq = []
        for entry in self.faq_data['general']:
            for (key, value) in entry.items():
                self.general_faq.append((key, value))
        self.twitch_faq = []
        for entry in self.faq_data['twitch']:
            for (key, value) in entry.items():
                self.twitch_faq.append((key, value))
        self.all_faq = self.general_faq + self.twitch_faq



    @commands.group(invoke_without_command=True)
    async def faq(self, ctx):
        '''
        Displays the FAQ menu of the bot.
        '''
        await self.show_faq(ctx, entries=self.all_faq)



    @faq.command(name='general')
    async def faq_general(self, ctx):
        '''
        Displays General FAQs
        '''
        await self.show_faq(ctx, entries=self.general_faq)



    @faq.command(name='twitch')
    async def faq_twitch(self, ctx):
        '''
        Displays Twitch FAQs
        '''
        await self.show_faq(ctx, entries=self.twitch_faq)





    async def show_faq(self, ctx, *, entries):
        avatar = ctx.author.avatar_url if ctx.author.avatar else ctx.author.default_avatar_url
        try:
            pages = FieldPages(ctx, entries=entries, per_page=1)
            pages.embed.set_author(name=f'Invoked by: {ctx.author}',
                                   icon_url=avatar)
            await pages.paginate()
        except Exception as e:
            await ctx.send(e)
        



def setup(bot):
    cog = FAQ(bot)
    bot.add_cog(cog)