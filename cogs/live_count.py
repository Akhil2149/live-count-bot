import aiohttp
from .utils import twitch_utils as tu
from .utils import db, main
from rethinkdb import RethinkDB

import discord
from discord.ext import commands, tasks



class LiveCount(commands.Cog):
    """The cog which does the live count stuff!"""

    def __init__(self, bot):
        self.bot = bot
        self.twitch_update_db.start()
        self.twitch_change_listener_task = self.bot.loop.create_task(self.twitch_change_listener())


    def cog_unload(self):
        self.twitch_update_db.stop()
        self.twitch_change_listener_task.cancel()


    @tasks.loop(seconds=5)
    async def twitch_update_db(self):
        '''
        Updates database for each streamer with followers and view data!

        Current Logic:
        --------------
        * Creates a while loop which works only if cog is loaded
        * Takes all the documents from the table
        * Loops over all the streamers
        *   Leaves if not connected to any channel(As there will be no client_id)
        *   Loops through all the connections and tries to get followers and views
        *   If able to get the informtion, update the document with it
        *   Break the loop 
        '''
        
        streamers = await self.r.table('twitch').run(self.connection)
        async for streamer in streamers:
            if not streamer.get("connections"):
                continue
            for connection in streamer["connections"]:
                guild = self.bot.get_guild(int(connection['guild']))
                client_id = await tu.get_twitch_client_id(guild=guild)
                try:
                    followers = await tu.get_twitch_followers(self.bot.session,
                                                              client_id,
                                                              streamer['id'])
                    views = await tu.get_twitch_views(self.bot.session,
                                                      client_id,
                                                      streamer['name'])
                except main.InvalidSecret:
                    continue
                await self.r.table('twitch'
                ).get(streamer['id']
                ).update({
                    'followers': followers,
                    'views': views
                }).run(self.connection)
                break
#                   !!! STILL UNCOMPLETE !!!



    async def twitch_change_listener(self):
        '''
        Takes the RethinkDB changefeed and validates it
        If valid, updates all channels in connections!
        '''
        stats = (
            "followers",
            "views"
        )
        await self.bot.wait_until_ready()
        r, connection = await db.acquire()
        async for document in r.table('twitch').changes().run(connection):
            before = document['before']
            after = document['after']

            changes = await main.have_changed(keys=stats, before=before, after=after)
            if not changes:
                continue

            display_name = after['display_name']
            name = after['name']
            for connection in after['connections']:
                if not connection['type'] in changes:
                    continue
                channel = self.bot.get_channel(int(connection['channel']))
                count = after[connection['type']]
                channel_name = connection['format'].format(count=count,
                                                           name=name,
                                                           display_name=display_name)
                try:
                    await channel.edit(name=channel_name)
                except:
                    continue



    @twitch_update_db.before_loop
    async def before_tasks(self):
        await self.bot.wait_until_ready()
        self.r, self.connection = await db.acquire()



def setup(bot):
    cog = LiveCount(bot)
    bot.add_cog(cog)
