import os
import json
from rethinkdb import RethinkDB
import asyncio

PATH = "database_defaults.json"

with open(PATH, encoding='utf-8', mode="r") as file:
    defaults = json.load(file)



async def acquire():
    '''
    Provides an asynchronous connection to database!

    Returns:
    -----------
    :class:`RethinkDB`
        The created instance of RethinkDB
    connection
        The connection created to the database
    '''
    r = RethinkDB()
    r.set_loop_type('asyncio')
    connection = await r.connect(db='live_count')
    return r, connection


async def check_default_guild_settings(guild, *, table="guilds"):
    r, connection = await acquire()
    default_settings = defaults[table]
    default_settings["id"] = str(guild.id)
    if not (await r.table(table).get(str(guild.id)).run(connection)):
        await r.table(table).insert(default_settings).run(connection)
