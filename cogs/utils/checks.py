from discord.ext import commands
import discord.utils

from cogs.utils.config import Config
import aiohttp

MAIN_GUILD_ID = None
PATREON_ROLE_IDS = []
#                    Lowest Tier    Middle Tier    High Tier



class InsufficientSupportingLevel(commands.CheckFailure):
    pass


class Checks:
    """
    Utility for various bot related checks
    Uses a number value check indicating various attributes of a user!

    2 = Upvoted on DBL
    4 = Lowest patreon tier
    8 = Middle level patreon tier
    16 = High level patreon tier
    """

    def __init__(self, bot):
        self.bot = bot
        self.config = Config()
        self.patreon_roles = []
        self.main_guild = None
        self.token = self.config.dbl_token
        self.base_url = "https://discordbots.org/api"
        self.headers = {"Authorization": self.token}
        self.session = None
        self.supporting_levels = {
            0: "None",
            2: "Upvoted on DBL",
            4: "the Lowest patreon tier",
            8: "a Middle level patreon tier",
            16: "a High level patreon tier"
        }
        self.task = self.bot.loop.create_task(self.initialize())   #Thanks Malarne


    def __unload(self):
        self.task.cancel()


    async def initialize(self):
        await self.bot.wait_until_ready()
        self.session = aiohttp.ClientSession()
        self.main_guild = self.bot.get_guild(MAIN_GUILD_ID)
        for role_id in PATREON_ROLE_IDS:
            role = self.main_guild.get_role(role_id)
            self.patreon_roles.append(role)



    def required_supporting_level(self, **value):
        """
        The actual decorator which can be used on functions

        Parameters
        ------------
        value: int
            The supporting level required for the user

  
        Raises
        ------------
        discord.ext.commands.CheckFailure.InsufficientSupportingLevel
            if the supporting level is less than value
        """
 
        async def predicate(ctx):
            author = ctx.author
            supporting_value = await self.get_supporting_level(ctx.author)
            if supporting_value >= value:
                return True
            else:
                raise InsufficientSupportingLevel(f'You must atleast have {self.supporting_levels[value]} to run this command!')

        return commands.check(predicate)


    async def get_supporting_level(self, user:discord.User):
        """
        The function which gets the current supporting level of the user


        Parameters
        ------------
        user: discord.User
            The user for whom the supporting level is returned


        Returns
        ------------
        supporting_value: int
            The current supporting level of the user

            2 = Upvoted on DBL
            4 = Lowest patreon tier
            8 = Middle level patreon tier
            16 = High level patreon tier
            
            returns the sum of all those values
        """

        supporting_value = 0    # default supporting level
        dbl_endpoint = f"bots/{self.bot.user.id}/check"
        dbl_params = {"userId": user.id}

        async with self.session.get(self.base_url+end_url, params=params, headers=self.headers) as response:
            data = await response.json()    # This gives {"voted": value} where value is 0 if not upvoted and 1 if upvoted

        if data["voted"]:
            supporting_value += 2

        member = self.main_guild.get_member(user.id)
        roles = member.roles

        if self.patreon_roles[0] in roles:    # Lowest Patreon Tier
            supporting_value += 4
        if self.patreon_roles[1] in roles:    # Middle level Patreon Tier
            supporting_value += 8
        if self.patreon_roles[2] in roles:    # High level Patreon Tier
            supporting_value += 16

        return supporting_value


    def return_human_form(self, supporting_value):
        """
        
        """
        str_values = []
        reverse_sv = sorted(self.supporting_levels.keys(),
                            reverse=True)
        for value in reverse_sv:
            if supporting_value == 0:
                break
            
            if supporting_value - value >= 0:
                supporting_value -= value
                str_values.append(
                    self.supporting_levels[value])
        return str_values

