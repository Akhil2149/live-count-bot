from . import main, config, db
from rethinkdb import RethinkDB
import discord


async def validate_twitch_client_id(session, client_id:str):
    '''
    Validates the client_id by hitting it to a dummy streamer

    Parameters:
    -----------
    session: aiohttp.ClientSession
        The session used to request with client_id
    client_id: str
        The twitch Client-ID which is to be validated

    Raises:
    -----------
    main.InvalidSecret
        If the client_id is wrong
    '''

    url = 'https://api.twitch.tv/helix/users/follows'
    parameters = {'to_id' : '47514919'}
    headers = {'Client-ID' : client_id}
    try:
        async with session.get(url, params=parameters, headers=headers) as response:
            await response.json()
    except:
        raise main.InvalidSecret()


async def get_twitch_streamer(ctx, client_id, streamer:str):
    """
    Checks if the streamer actually exists.
    If it does then adds it to the database

    Parameters:
    -----------
    ctx: commands.Context
        The command context
    client_id: :class:`str`
        The twitch client_id used to get the streamer info
    streamer: :class:`str`
        The login name of the streamer!

    Returns:
    -----------

    """
    streamer = streamer.lower()
    await ctx.acquire()
    querys = await ctx.r.table('twitch'
            ).filter(ctx.r.row['name']==streamer
            ).run(ctx.connection)
    # query = await list(query)
    async for query in querys:
        if query:
            return query


    url = 'https://api.twitch.tv/helix/users'
    params = {'login': streamer}
    headers = {'Client-ID': client_id}
    async with ctx.bot.session.get(url, params=params, headers=headers) as resp:
        jsondata = await resp.json()

    insertion = {
        'name': streamer,
        'display_name': jsondata['data'][0]['display_name'],
        'id': jsondata['data'][0]['id'],
        'channels': {}
    }
    try:
        await ctx.r.table('twitch').insert(insertion).run(ctx.connection)
        return insertion
    finally:
        await ctx.release()





async def get_twitch_followers(session, client_id:str, streamer_id:int):
    '''
    Gets the twitch followers of the streamer_id!

    Parameters:
    -----------    
    session: aiohttp.ClientSession
        The session used to request with client_id
    client_id: str
        The client_id which will be used to get the data
    streamer_id: int
        The streamer_id of the streamer whose followers
        are requested!

    Returns:
    ----------
    followers: int
        The number of followers the streamer_id has
    '''

    url = 'https://api.twitch.tv/helix/users/follows'
    params = {'to_id': streamer_id}
    headers = {'Client-ID': client_id}

    try:
        async with session.get(url, params=params, headers=headers) as resp:
            resp.raise_for_status()
            followersdata = await resp.json()
    except:
        raise main.InvalidSecret()
    followers = followersdata["total"]
    return followers


async def get_twitch_views(session, client_id:int, streamer:str):
    '''
    Gets the twitch views of the streamer!

    Parameters:
    -----------
    session: aiohttp.ClientSession
        The session used to request with client_id
    client_id: str
        The client_id which will be used to get the data
    streamer: str
        The streamer whose views are requested!

    Returns:
    ----------
    views: int
        The number of views the streamer has
    '''

    url = 'https://api.twitch.tv/helix/users'
    params = {'login': streamer}
    headers = {'Client-ID': client_id}

    try:
        async with session.get(url, params=params, headers=headers) as resp:
            resp.raise_for_status()
            viewsdata = await resp.json()
    except:
        raise main.InvalidSecret()
    views = viewsdata["data"][0]["view_count"]
    return views


async def get_twitch_client_id(guild:discord.Guild=None):
    """
    Don't use if already querying the database for something!
    
    Parameters:
    -----------
    guild: discord.Guild
        Guild of which the Client-ID is requested!
        If None, then global Client-ID is returned

    Returns:
    -----------
    client_id: The requested client_id

    Raises:
    -----------
    main.MissingSecret:
        Raised if no Client-ID is set!
    """
    if not guild:
        return config.twitch_token

    r, connection = await db.acquire()
    client_id = await r.table("guilds"
                           ).get(str(guild.id)
                           ).pluck({"secrets":{"twitch": True}}
                           )["secrets"]["twitch"].run(connection)
    await connection.close()
    if not client_id:
        raise main.MissingSecret("Twitch Client-ID not set!")

    return client_id