from discord.ext import commands
import asyncio

from random import choice, randint

from . import db

import discord
from discord.ext import commands

from rethinkdb import RethinkDB




class _ContextDBAcquire:
    __slots__ = ('ctx',)

    def __init__(self, ctx):
        self.ctx = ctx

    def __await__(self):
        return self.ctx._acquire().__await__()

    async def __aenter__(self):
        await self.ctx._acquire()
        return self.ctx.r, self.ctx.connection

    async def __aexit__(self, *args):
        await self.ctx.release()


class Context(commands.Context):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._r = None
        self._connection = None

    def __repr__(self):
        # we need this for our cache key strategy
        return '<Context>'

    @property
    def session(self):
        return self.bot.session

    @property
    def r(self):
        return self._r

    @property
    def connection(self):
        return self._connection
    

    async def _acquire(self):
        if self._r is None:
            self._r, self._connection = await db.acquire()
        return self._r, self._connection

    def acquire(self):
        """Acquires a database connection. e.g. ::

            async with ctx.acquire():
                await ctx.r.query.run(ctx.connection)

        or: ::

            await ctx.acquire()
            try:
                await ctx.r.query().run(ctx.connection)
            finally:
                await ctx.release()
        """
        return _ContextDBAcquire(self)

    async def release(self):
        """Releases the database connection!

        Useful if needed for "long" interactive commands where
        we want to release the connection and re-acquire later.

        Otherwise, this is called automatically by the bot.
        """
        # from source digging asyncpg source, releasing an already
        # released connection does nothing

        if self._connection is not None:
            await self._connection.close(noreply_wait=True)
            self._db = None
            self._connection = None



    def is_int(self):
        try:
            int(self.message.content)
        except ValueError:
            return False
        else:
            return True


    @staticmethod
    def random_color():
        '''
        Gets a random discord.Color object

        Returns:
        -----------
        color: discord.Color
            The randomly generated color
        '''
        color = ''.join([choice('0123456789ABCDEF') for x in range(6)])
        color = int(color, 16)
        color = discord.Color(value=color)
        return color

